'use strict';

/**
 * @ngdoc function
 * @name ngTaxiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ngTaxiApp
 */
angular.module('ngTaxiApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
