'use strict';

angular.module('ngTaxiApp')
    .controller('CarsCtrl', ['$location','locationFactory',function($location, locationFactory){
        this.title = 'car ctrl';

        this.showLocationChoser = false;

        this.activate = function(carId){
            console.log('car with id '+ carId + ' activated!');
            this.showLocationChoser = true;
            // var obj = locationFactory.get().$promise.then(function(data) {
            //     return data.locations;
            // });
            // var obj2 = obj[Object.keys(obj)[1]];
            // var locationList = obj2[Object.keys(obj2)[1]];
            // console.log(locationList);
            // console.log(locationList.length);
            // 
            // locationFactory.getAllLocations(function(data){
            //     console.log('at start of callback function');                
            //     console.log(data);
            //     
            //     var locationList = data.locations;                
            //     for(var i = 0; i< locationList.length; i++){
            //         console.log(locationList[i].code);
            //     }                
            //     console.log('finish of callback function');
            // });
            // 
            // console.log('lol');            
        };

        this.applyLocation = function(locationId){
            console.log('location applied '+ locationId);
            $location.path('/chose_order');
        };

        this.register = function(car){
            console.log('he wants to register car '+ car);
            this.showLocationChoser = true;
        };
    }]);
