'use strict';

/**
 * @ngdoc function
 * @name ngTaxiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ngTaxiApp
 */
angular.module('ngTaxiApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
