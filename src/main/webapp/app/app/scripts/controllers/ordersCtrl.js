'use strict';

  angular.module('ngTaxiApp')
    .controller('OrdersCtrl', ['$location', function($location) {
      this.showForm = true;
      
      this.send = function(order){
          console.log(order);
          this.showForm = false;
          //$location.path('/about');
      };
      
      this.title = 'order controller title';      
      
      this.accept = function(order){
        console.log('order accepted'+order);
        $location.path('/wait_for_car');
      };
      
      this.reject = function(order){
        console.log('order rejected'+order);
        $location.path('/');  
      };
      
      this.receivedOrder = {
          title: 'order title',
          number: 213123123
      };
      
      this.list = [
        {
            title : 'ord1',
            price : 212
        },
        {
            title : 'ord2',
            price : 129
        }  
      ];
      
      this.acceptOrderByDriver = function(order){
          console.log(order);
          $location.path('/implementing_order');
      };
      
      this.completeOrder = function(){
          console.log('order completed');
          $location.path('/chose_order');
      };
      
      this.updateLocation = function(locationId){
          console.log('location is now: '+ locationId);
      };
      
    }]);

