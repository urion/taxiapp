'use strict';

/**
 * @ngdoc overview
 * @name ngTaxiApp
 * @description
 * # ngTaxiApp
 *
 * Main module of the application.
 */
angular
  .module('ngTaxiApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/enter.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/create_order', {
        templateUrl: 'views/createOrder.html',
        controller: 'OrdersCtrl',
        controllerAs: 'orders'
      })
      .when('/wait_for_car', {
        templateUrl: 'views/waitForCar.html',
        controller: 'OrdersCtrl',
        controllerAs: 'orders'
      })
      .when('/chose_order', {
        templateUrl: 'views/choseOrder.html',
        controller: 'OrdersCtrl',
        controllerAs: 'orders'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/login_car', {
        templateUrl: 'views/loginCar.html',
        controller: 'CarsCtrl',
        controllerAs: 'cars'
      })
      .when('/register_car', {
        templateUrl: 'views/registerCar.html',
        controller: 'CarsCtrl',
        controllerAs: 'cars'
      })
      .when('/implementing_order', {
        templateUrl: 'views/implementingOrder.html',
        controller: 'OrdersCtrl',
        controllerAs: 'orders'  
      })
      .otherwise({
        redirectTo: '/'
      });
  });
