'use strict';
angular.module('ngTaxiApp')
.factory('carsService', function($http, $resource) {
    var service = {};
    service.activateCar = function(id /*,location */) {
        return $http.post('/app/activatecar', 'id=' + id,{
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(/*data*/) {
            console.log('login successful');
            // localStorage.setItem('session', {});
        }, function(/*data*/) {
            console.log('error logging in');
        });
    };
    service.register = function(car, success, failure) {
        $resource('/app/taxies')
            .save({}, car, success, failure);
    };
    service.getCarById = function(carId) {
        return $resource('/app/taxies/:paramCarId')
            .get({paramCarId:carId}).$promise;
    };
    return service;
    // service.userExists = function(account, success, failure) {
    //     var Account = $resource('/basic-web-app/rest/accounts');
    //     var data = Account.get({name:account.name, password:account.password}, function() {
    //         var accounts = data.accounts;
    //         if(accounts.length !== 0) {
    //             success(account);
    //         } else {
    //             failure();
    //         }
    //     },
    //     failure);
    // };
    // service.getAllAccounts = function() {
    //       var Account = $resource('/basic-web-app/rest/accounts');
    //       return Account.get().$promise.then(function(data) {
    //         return data.accounts;
    //       });
    //   };
});



    