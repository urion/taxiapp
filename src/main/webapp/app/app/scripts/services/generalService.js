'use strict';
angular.module('ngTaxiApp')
    .factory('locationFactory', function($resource) {
        var service  = {};
        service.getAllLocations = function(callback) {
            return $resource('/taxi-app/general/alllocations')
                        .get(callback);        
        };
        service.getAllRoads = function(callback) {
            return $resource('/taxi-app/general/allroads')
                        .get(callback);  
        };
        return service;
    });