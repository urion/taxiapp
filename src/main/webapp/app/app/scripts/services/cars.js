angular.module('ngTaxiApp')
  .factory('CarFactory', function CarFactory ($q, $http) {
    'use strict';
    var exports = {};

    exports.cars = [];

    // exports.goToMessage = function(id) {
    //   if ( angular.isNumber(id) ) {
    //     console.log('inbox/email/' + id)
    //     $location.path('inbox/email/' + id)
    //   }
    // }

    exports.deleteCar = function (id, index) {
      this.cars.splice(index, 1);
    };

    exports.getCars = function () {
        
      var deferred = $q.defer();
      
      $http.get('json/cars.json')
        .success(function (data) {
          exports.cars = data;
          deferred.resolve(data);
        })
        .error(function (data) {
          deferred.reject(data);
        });
        
      return deferred.promise;
    };

    return exports;
  });