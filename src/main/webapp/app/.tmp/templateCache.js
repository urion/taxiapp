angular.module('ngTaxiApp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/about.html',
    "<p>This is the about view.</p>"
  );


  $templateCache.put('views/choseOrder.html',
    "<p>Chose order view</p> <p> {{orders.title}} </p> <div class=\"input-group\" ng-repeat=\"order in orders.list\"> <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\" value=\"{{order.title}} {{order.price}}\" readonly> <span class=\"input-group-btn\"> <button class=\"btn btn-default\" type=\"button\" ng-click=\"orders.acceptOrderByDriver(order)\">Accept!</button> </span> </div>"
  );


  $templateCache.put('views/createOrder.html',
    "<div> <form novalidate class=\"simple-form\" ng-show=\"orders.showForm\"> Location From <input type=\"text\" ng-model=\"order.from\"><br> Location To <input type=\"text\" ng-model=\"order.to\"><br> Your name <input type=\"text\" ng-model=\"order.clientName\"><br> <input type=\"submit\" ng-click=\"orders.send(order)\" value=\"Save\"> </form> <div ng-hide=\"orders.showForm\"> <pre>\n" +
    "        order = {{order | json}}\n" +
    "    </pre> <div class=\"btn-group btn-group\" role=\"group\"> <button class=\"btn btn-default\" ng-click=\"orders.accept(orders.receivedOrder)\"> Accept </button> <button class=\"btn btn-default\" ng-click=\"orders.reject(orders.receivedOrder)\"> Reject </button> </div> </div> </div>"
  );


  $templateCache.put('views/enter.html',
    "<div class=\"btn-group btn-group-lg\" role=\"group\" aria-label=\"...\"> <!--<button type=\"button\" class=\"btn btn-default\">I am client</button>--> <a href=\"#/create_order\" class=\"btn btn-default\" role=\"button\">I am client</a> <div class=\"btn-group btn-group-lg\" role=\"group\"> <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> I am driver <span class=\"caret\"></span> </button> <ul class=\"dropdown-menu\"> <li><a href=\"#/login_car\">Already registered</a></li> <li><a href=\"#/register_car\">Wanna register</a></li> </ul> </div> </div>"
  );


  $templateCache.put('views/implementingOrder.html',
    "<p>{{orders.title}}</p> <p>implementing order view</p> <button role=\"button\" class=\"btn btn-default\" ng-click=\"orders.completeOrder()\"> Complete order! </button> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"locationId\" placeholder=\"Search for...\" value=\"location\"> <span class=\"input-group-btn\"> <button class=\"btn btn-default\" type=\"button\" ng-click=\"orders.updateLocation(locationId)\">Update location!</button> </span> </div>"
  );


  $templateCache.put('views/loginCar.html',
    "<p>login car view</p> <p>{{cars.title}}</p> <form ng-hide=\"cars.showLocationChoser\"> <fieldset class=\"form-group\"> <label for=\"inputCarId\">Your Car Id</label> <input type=\"text\" ng-model=\"carId\" class=\"form-control\" placeholder=\"Enter your id\"> <small class=\"text-muted\">It is used to authenticate you</small> </fieldset> <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"cars.activate(carId)\">Start work!</button> </form> <form ng-show=\"cars.showLocationChoser\"> <fieldset class=\"form-group\"> <label for=\"locationId\">Where are you now?</label> <input type=\"text\" ng-model=\"locationId\" class=\"form-control\" placeholder=\"Enter your location\"> <small class=\"text-muted\">System needs to know where you are</small> </fieldset> <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"cars.applyLocation(locationId)\">Here i am!</button> </form>"
  );


  $templateCache.put('views/registerCar.html',
    "<p>register car view </p> <form ng-hide=\"cars.showLocationChoser\"> <fieldset class=\"form-group\"> <label for=\"description\">Your Car Description</label> <input type=\"text\" ng-model=\"car.driverName\" class=\"form-control\" placeholder=\"Enter your Name\"> <small class=\"text-muted\">How should we call you, monsieur?</small> </fieldset> <fieldset class=\"form-group\"> <label for=\"inputCarId\">Your Car Id</label> <input type=\"text\" ng-model=\"car.description\" class=\"form-control\" placeholder=\"Enter your car description\"> <small class=\"text-muted\">Describe your car for clients</small> </fieldset> <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"cars.register(car)\">Start work!</button> </form> <form ng-show=\"cars.showLocationChoser\"> <fieldset class=\"form-group\"> <label for=\"locationId\">Where are you now?</label> <input type=\"text\" ng-model=\"locationId\" class=\"form-control\" placeholder=\"Enter your location\"> <small class=\"text-muted\">System needs to know where you are</small> </fieldset> <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"cars.applyLocation(locationId)\">Here i am!</button> </form>"
  );


  $templateCache.put('views/waitForCar.html',
    "<p>Waiting for car view.</p> <p>{{orders.title}}</p> <br> <button class=\"btn btn-default\" ng-click=\"orders.reject(orders.receivedOrder)\"> Reject </button>"
  );

}]);
