'use strict';

/**
 * @ngdoc overview
 * @name ngTaxiApp
 * @description
 * # ngTaxiApp
 *
 * Main module of the application.
 */
angular
  .module('ngTaxiApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(["$routeProvider", function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/enter.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/create_order', {
        templateUrl: 'views/createOrder.html',
        controller: 'OrdersCtrl',
        controllerAs: 'orders'
      })
      .when('/wait_for_car', {
        templateUrl: 'views/waitForCar.html',
        controller: 'OrdersCtrl',
        controllerAs: 'orders'
      })
      .when('/chose_order', {
        templateUrl: 'views/choseOrder.html',
        controller: 'OrdersCtrl',
        controllerAs: 'orders'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/login_car', {
        templateUrl: 'views/loginCar.html',
        controller: 'CarsCtrl',
        controllerAs: 'cars'
      })
      .when('/register_car', {
        templateUrl: 'views/registerCar.html',
        controller: 'CarsCtrl',
        controllerAs: 'cars'
      })
      .when('/implementing_order', {
        templateUrl: 'views/implementingOrder.html',
        controller: 'OrdersCtrl',
        controllerAs: 'orders'  
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);

'use strict';

/**
 * @ngdoc function
 * @name ngTaxiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ngTaxiApp
 */
angular.module('ngTaxiApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name ngTaxiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ngTaxiApp
 */
angular.module('ngTaxiApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

  angular.module('ngTaxiApp')
    .controller('OrdersCtrl', ['$location', function($location) {
      this.showForm = true;
      
      this.send = function(order){
          console.log(order);
          this.showForm = false;
          //$location.path('/about');
      };
      
      this.title = 'order controller title';      
      
      this.accept = function(order){
        console.log('order accepted'+order);
        $location.path('/wait_for_car');
      };
      
      this.reject = function(order){
        console.log('order rejected'+order);
        $location.path('/');  
      };
      
      this.receivedOrder = {
          title: 'order title',
          number: 213123123
      };
      
      this.list = [
        {
            title : 'ord1',
            price : 212
        },
        {
            title : 'ord2',
            price : 129
        }  
      ];
      
      this.acceptOrderByDriver = function(order){
          console.log(order);
          $location.path('/implementing_order');
      };
      
      this.completeOrder = function(){
          console.log('order completed');
          $location.path('/chose_order');
      };
      
      this.updateLocation = function(locationId){
          console.log('location is now: '+ locationId);
      };
      
    }]);


'use strict';

angular.module('ngTaxiApp')
    .controller('CarsCtrl', ['$location','locationFactory',function($location, locationFactory){
        this.title = 'car ctrl';

        this.showLocationChoser = false;

        this.activate = function(carId){
            console.log('car with id '+ carId + ' activated!');
            this.showLocationChoser = true;
            // var obj = locationFactory.get().$promise.then(function(data) {
            //     return data.locations;
            // });
            // var obj2 = obj[Object.keys(obj)[1]];
            // var locationList = obj2[Object.keys(obj2)[1]];
            // console.log(locationList);
            // console.log(locationList.length);
            // 
            locationFactory.getAllLocations(function(data){
                console.log('at start of callback function');                
                console.log(data);
                
                var locationList = data.locations;                
                for(var i = 0; i< locationList.length; i++){
                    console.log(locationList[i].code);
                }                
                console.log('finish of callback function');
            });
            
            console.log('lol');            
        };

        this.applyLocation = function(locationId){
            console.log('location applied '+ locationId);
            $location.path('/chose_order');
        };

        this.register = function(car){
            console.log('he wants to register car '+ car);
            this.showLocationChoser = true;
        };
    }]);

'use strict';
angular.module('ngTaxiApp')
.factory('carsService', ["$http", "$resource", function($http, $resource) {
    var service = {};
    service.activateCar = function(id /*,location */) {
        return $http.post('/app/activatecar', 'id=' + id,{
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(/*data*/) {
            console.log('login successful');
            // localStorage.setItem('session', {});
        }, function(/*data*/) {
            console.log('error logging in');
        });
    };
    service.register = function(car, success, failure) {
        $resource('/app/taxies')
            .save({}, car, success, failure);
    };
    service.getCarById = function(carId) {
        return $resource('/app/taxies/:paramCarId')
            .get({paramCarId:carId}).$promise;
    };
    return service;
    // service.userExists = function(account, success, failure) {
    //     var Account = $resource('/basic-web-app/rest/accounts');
    //     var data = Account.get({name:account.name, password:account.password}, function() {
    //         var accounts = data.accounts;
    //         if(accounts.length !== 0) {
    //             success(account);
    //         } else {
    //             failure();
    //         }
    //     },
    //     failure);
    // };
    // service.getAllAccounts = function() {
    //       var Account = $resource('/basic-web-app/rest/accounts');
    //       return Account.get().$promise.then(function(data) {
    //         return data.accounts;
    //       });
    //   };
}]);



    
'use strict';
angular.module('ngTaxiApp')
    .factory('locationFactory', ["$resource", function($resource) {
        var factory  = {};
        factory.getAllLocations = function(callback) {
            return $resource('/taxi-app/general/alllocations')
                        .get(callback);        
        };        
        return factory;
    }]);
angular.module('ngTaxiApp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/about.html',
    "<p>This is the about view.</p>"
  );


  $templateCache.put('views/choseOrder.html',
    "<p>Chose order view</p> <p> {{orders.title}} </p> <div class=\"input-group\" ng-repeat=\"order in orders.list\"> <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\" value=\"{{order.title}} {{order.price}}\" readonly> <span class=\"input-group-btn\"> <button class=\"btn btn-default\" type=\"button\" ng-click=\"orders.acceptOrderByDriver(order)\">Accept!</button> </span> </div>"
  );


  $templateCache.put('views/createOrder.html',
    "<div> <form novalidate class=\"simple-form\" ng-show=\"orders.showForm\"> Location From <input type=\"text\" ng-model=\"order.from\"><br> Location To <input type=\"text\" ng-model=\"order.to\"><br> Your name <input type=\"text\" ng-model=\"order.clientName\"><br> <input type=\"submit\" ng-click=\"orders.send(order)\" value=\"Save\"> </form> <div ng-hide=\"orders.showForm\"> <pre>\n" +
    "        order = {{order | json}}\n" +
    "    </pre> <div class=\"btn-group btn-group\" role=\"group\"> <button class=\"btn btn-default\" ng-click=\"orders.accept(orders.receivedOrder)\"> Accept </button> <button class=\"btn btn-default\" ng-click=\"orders.reject(orders.receivedOrder)\"> Reject </button> </div> </div> </div>"
  );


  $templateCache.put('views/enter.html',
    "<div class=\"btn-group btn-group-lg\" role=\"group\" aria-label=\"...\"> <!--<button type=\"button\" class=\"btn btn-default\">I am client</button>--> <a href=\"#/create_order\" class=\"btn btn-default\" role=\"button\">I am client</a> <div class=\"btn-group btn-group-lg\" role=\"group\"> <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> I am driver <span class=\"caret\"></span> </button> <ul class=\"dropdown-menu\"> <li><a href=\"#/login_car\">Already registered</a></li> <li><a href=\"#/register_car\">Wanna register</a></li> </ul> </div> </div>"
  );


  $templateCache.put('views/implementingOrder.html',
    "<p>{{orders.title}}</p> <p>implementing order view</p> <button role=\"button\" class=\"btn btn-default\" ng-click=\"orders.completeOrder()\"> Complete order! </button> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"locationId\" placeholder=\"Search for...\" value=\"location\"> <span class=\"input-group-btn\"> <button class=\"btn btn-default\" type=\"button\" ng-click=\"orders.updateLocation(locationId)\">Update location!</button> </span> </div>"
  );


  $templateCache.put('views/loginCar.html',
    "<p>login car view</p> <p>{{cars.title}}</p> <form ng-hide=\"cars.showLocationChoser\"> <fieldset class=\"form-group\"> <label for=\"inputCarId\">Your Car Id</label> <input type=\"text\" ng-model=\"carId\" class=\"form-control\" placeholder=\"Enter your id\"> <small class=\"text-muted\">It is used to authenticate you</small> </fieldset> <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"cars.activate(carId)\">Start work!</button> </form> <form ng-show=\"cars.showLocationChoser\"> <fieldset class=\"form-group\"> <label for=\"locationId\">Where are you now?</label> <input type=\"text\" ng-model=\"locationId\" class=\"form-control\" placeholder=\"Enter your location\"> <small class=\"text-muted\">System needs to know where you are</small> </fieldset> <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"cars.applyLocation(locationId)\">Here i am!</button> </form>"
  );


  $templateCache.put('views/registerCar.html',
    "<p>register car view </p> <form ng-hide=\"cars.showLocationChoser\"> <fieldset class=\"form-group\"> <label for=\"description\">Your Car Description</label> <input type=\"text\" ng-model=\"car.driverName\" class=\"form-control\" placeholder=\"Enter your Name\"> <small class=\"text-muted\">How should we call you, monsieur?</small> </fieldset> <fieldset class=\"form-group\"> <label for=\"inputCarId\">Your Car Id</label> <input type=\"text\" ng-model=\"car.description\" class=\"form-control\" placeholder=\"Enter your car description\"> <small class=\"text-muted\">Describe your car for clients</small> </fieldset> <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"cars.register(car)\">Start work!</button> </form> <form ng-show=\"cars.showLocationChoser\"> <fieldset class=\"form-group\"> <label for=\"locationId\">Where are you now?</label> <input type=\"text\" ng-model=\"locationId\" class=\"form-control\" placeholder=\"Enter your location\"> <small class=\"text-muted\">System needs to know where you are</small> </fieldset> <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"cars.applyLocation(locationId)\">Here i am!</button> </form>"
  );


  $templateCache.put('views/waitForCar.html',
    "<p>Waiting for car view.</p> <p>{{orders.title}}</p> <br> <button class=\"btn btn-default\" ng-click=\"orders.reject(orders.receivedOrder)\"> Reject </button>"
  );

}]);
