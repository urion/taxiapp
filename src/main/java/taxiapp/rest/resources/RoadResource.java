package taxiapp.rest.resources;

import org.springframework.hateoas.ResourceSupport;
import taxiapp.core.models.entities.Location;
import taxiapp.core.models.entities.Road;
import taxiapp.rest.mvc.LocationInfoProvider;

/**
 * Created by urion on 22.11.15.
 */
public class RoadResource extends ResourceSupport {
    private int codeStart;
    private int codeFinish;
    private double length;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public int getCodeStart() {
        return codeStart;
    }

    public void setCodeStart(int codeStart) {
        this.codeStart = codeStart;
    }

    public int getCodeFinish() {
        return codeFinish;
    }

    public void setCodeFinish(int codeFinish) {
        this.codeFinish = codeFinish;
    }

    public Road toRoad() {
        Road road = new Road();
//        Location lStart = new Location();
//        lStart.setCode(codeStart);
//        Location lFinish = new Location();
//        lFinish.setCode(codeFinish);
        road.setLength(length);

        Location locationStart = LocationInfoProvider
                .getLocationService()
                .getLocationByLocationCode(codeStart);
        road.setStart(locationStart);

        Location locationFinish = LocationInfoProvider
                .getLocationService()
                .getLocationByLocationCode(codeStart);
        road.setFinish(locationFinish);

        return road;
    }
}
