package taxiapp.rest.resources;

import org.springframework.hateoas.ResourceSupport;
import taxiapp.core.models.entities.TaxiOrder;

/**
 * Created by urion on 29.10.15.
 */
public class OrderResource extends ResourceSupport {
    private long carId;
    private int status;
    private double price;
    private String clientName;
    private int locationCodeFrom;
    private int locationCodeTo;

    public int getLocationCodeTo() {
        return locationCodeTo;
    }

    public void setLocationCodeTo(int locationCodeTo) {
        this.locationCodeTo = locationCodeTo;
    }

    public int getLocationCodeFrom() {
        return locationCodeFrom;
    }

    public void setLocationCodeFrom(int locationCodeFrom) {
        this.locationCodeFrom = locationCodeFrom;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public TaxiOrder toOrder() {
        TaxiOrder taxiOrder = new TaxiOrder();
        taxiOrder.setStatus(status);
        taxiOrder.setPrice(price);
        taxiOrder.setClientName(clientName);
        taxiOrder.setLocationCodeFrom(locationCodeFrom);
        taxiOrder.setLocationCodeTo(locationCodeTo);
        return taxiOrder;
    }
}
