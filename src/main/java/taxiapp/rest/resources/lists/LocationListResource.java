package taxiapp.rest.resources.lists;

import org.springframework.hateoas.ResourceSupport;
import taxiapp.rest.resources.LocationResource;

import java.util.List;

/**
 * Created by urion on 23.11.15.
 */
public class LocationListResource extends ResourceSupport {
    private List<LocationResource> locations;

    public List<LocationResource> getLocations() {
        return locations;
    }

    public void setLocations(List<LocationResource> locations) {
        this.locations = locations;
    }
}
