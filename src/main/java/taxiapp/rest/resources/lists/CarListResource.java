package taxiapp.rest.resources.lists;

import org.springframework.hateoas.ResourceSupport;
import taxiapp.rest.resources.CarResource;

import java.util.List;

/**
 * Created by urion on 23.11.15.
 */
public class CarListResource extends ResourceSupport {
    private List<CarResource> cars;

    public List<CarResource> getCars() {
        return cars;
    }

    public void setCars(List<CarResource> cars) {
        this.cars = cars;
    }
}
