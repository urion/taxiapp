package taxiapp.rest.resources.lists;

import org.springframework.hateoas.ResourceSupport;
import taxiapp.rest.resources.OrderResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by urion on 22.11.15.
 */
public class OrdersListResource extends ResourceSupport {

    private List<OrderResource> orders = new ArrayList<>();

    public List<OrderResource> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderResource> orders) {
        this.orders = orders;
    }
}
