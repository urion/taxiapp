package taxiapp.rest.resources.lists;

import org.springframework.hateoas.ResourceSupport;
import taxiapp.rest.resources.RoadResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by urion on 23.11.15.
 */
public class RoadListResource extends ResourceSupport {

    private List<RoadResource> roads = new ArrayList<>();

    public List<RoadResource> getRoads() {
        return roads;
    }

    public void setRoads(List<RoadResource> roads) {
        this.roads = roads;
    }
}
