package taxiapp.rest.resources;


import org.springframework.hateoas.ResourceSupport;
import taxiapp.core.models.entities.Location;
import taxiapp.rest.mvc.LocationInfoProvider;

/**
 * Created by urion on 10.11.15.
 */
public class LocationResource extends ResourceSupport{
    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Location toLocation(){
//        Location location = new Location();
//        location.setCode(code);
        Location location = LocationInfoProvider
                .getLocationService()
                .getLocationByLocationCode(code);
        return location;
    }
}
