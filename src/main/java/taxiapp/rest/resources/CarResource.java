package taxiapp.rest.resources;

import org.springframework.hateoas.ResourceSupport;
import taxiapp.core.models.entities.Car;
import taxiapp.core.models.entities.Location;
import taxiapp.rest.mvc.LocationInfoProvider;


public class CarResource extends ResourceSupport {
    private String description;
    private String driverName;
    private int locationCode;
    private int status;

    public int getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(int locationCode) {
        this.locationCode = locationCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public int getLocation() {
        return locationCode;
    }

    public void setLocation(Location location) {
        this.locationCode = location.getCode();
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Car toCar(){
        Car car = new Car();

        car.setDescription(description);
        car.setDriverName(driverName);
        car.setStatus(status);
//        Location location = new Location();
//        location.setCode(locationCode);
        Location location = LocationInfoProvider
                .getLocationService()
                .getLocationByLocationCode(locationCode);
        car.setLocation(location);
        return car;
    }
}
