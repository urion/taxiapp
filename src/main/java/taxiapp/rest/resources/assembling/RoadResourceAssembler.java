package taxiapp.rest.resources.assembling;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import taxiapp.core.models.entities.Road;
import taxiapp.rest.resources.RoadResource;

/**
 * Created by urion on 23.11.15.
 */
public class RoadResourceAssembler extends ResourceAssemblerSupport<Road, RoadResource> {
    public RoadResourceAssembler() {
        super(Road.class, RoadResource.class);
    }

    @Override
    public RoadResource toResource(Road road) {
        RoadResource resource = new RoadResource();
        resource.setCodeStart(road.getStart().getCode());
        resource.setCodeFinish(road.getFinish().getCode());
        resource.setLength(road.getLength());
        return resource;
    }
}
