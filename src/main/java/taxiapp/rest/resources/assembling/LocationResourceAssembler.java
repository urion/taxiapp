package taxiapp.rest.resources.assembling;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import taxiapp.core.models.entities.Location;
import taxiapp.rest.resources.LocationResource;

/**
 * Created by urion on 10.11.15.
 */
public class LocationResourceAssembler extends ResourceAssemblerSupport<Location,LocationResource> {
    public LocationResourceAssembler(){
        super(Location.class,LocationResource.class);
    }
    @Override
    public LocationResource toResource(Location location) {
        LocationResource resource = new LocationResource();
        resource.setCode(location.getCode());

        //resource.add(linkTo(methodOn(TaxiesController.class).getCar(car.getId())).withSelfRel());

        return resource;
    }
}
