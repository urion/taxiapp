package taxiapp.rest.resources.assembling.lists;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import taxiapp.core.services.util.LocationList;
import taxiapp.rest.mvc.GeneralController;
import taxiapp.rest.resources.assembling.LocationResourceAssembler;
import taxiapp.rest.resources.lists.LocationListResource;

/**
 * Created by urion on 23.11.15.
 */
public class LocationListResourceAssembler extends ResourceAssemblerSupport<LocationList, LocationListResource> {
    public LocationListResourceAssembler() {
        super(GeneralController.class, LocationListResource.class);
    }

    @Override
    public LocationListResource toResource(LocationList locationList) {
        LocationListResource resource = new LocationListResource();
        resource.setLocations(new LocationResourceAssembler().toResources(locationList.getLocations()));
        return resource;
    }
}
