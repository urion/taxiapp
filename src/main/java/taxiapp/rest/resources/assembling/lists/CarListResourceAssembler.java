package taxiapp.rest.resources.assembling.lists;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import taxiapp.core.services.util.CarList;
import taxiapp.rest.mvc.TaxiesController;
import taxiapp.rest.resources.assembling.CarResourceAssembler;
import taxiapp.rest.resources.lists.CarListResource;

/**
 * Created by urion on 23.11.15.
 */
public class CarListResourceAssembler extends ResourceAssemblerSupport<CarList, CarListResource> {

    public CarListResourceAssembler() {
        super(TaxiesController.class, CarListResource.class);
    }

    @Override
    public CarListResource toResource(CarList carList) {
        CarListResource resource = new CarListResource();
        resource.setCars(new CarResourceAssembler().toResources(carList.getCars()));
        return resource;
    }
}
