package taxiapp.rest.resources.assembling.lists;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import taxiapp.core.services.util.RoadList;
import taxiapp.rest.mvc.GeneralController;
import taxiapp.rest.resources.assembling.RoadResourceAssembler;
import taxiapp.rest.resources.lists.RoadListResource;

/**
 * Created by urion on 23.11.15.
 */
public class RoadListResourceAssembler extends ResourceAssemblerSupport<RoadList, RoadListResource> {
    public RoadListResourceAssembler() {
        super(GeneralController.class, RoadListResource.class);
    }

    @Override
    public RoadListResource toResource(RoadList roadList) {
        RoadListResource resource = new RoadListResource();
        resource.setRoads(new RoadResourceAssembler().toResources(roadList.getRoads()));
        return resource;
    }
}
