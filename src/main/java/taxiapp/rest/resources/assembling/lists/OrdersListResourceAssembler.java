package taxiapp.rest.resources.assembling.lists;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import taxiapp.core.services.util.OrdersList;
import taxiapp.rest.mvc.OrdersController;
import taxiapp.rest.resources.assembling.OrderResourceAssembler;
import taxiapp.rest.resources.lists.OrdersListResource;

/**
 * Created by urion on 22.11.15.
 */
public class OrdersListResourceAssembler extends ResourceAssemblerSupport<OrdersList, OrdersListResource> {

    public OrdersListResourceAssembler() {
        super(OrdersController.class, OrdersListResource.class);
    }

    @Override
    public OrdersListResource toResource(OrdersList ordersList) {
        OrdersListResource resource = new OrdersListResource();
        resource.setOrders(new OrderResourceAssembler().toResources(ordersList.getTaxiOrders()));
        return resource;
    }
}
