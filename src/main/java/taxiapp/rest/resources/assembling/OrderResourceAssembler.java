package taxiapp.rest.resources.assembling;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import taxiapp.core.models.entities.TaxiOrder;
import taxiapp.rest.mvc.OrdersController;
import taxiapp.rest.resources.OrderResource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
/**
 * Created by urion on 29.10.15.
 */
public class OrderResourceAssembler extends ResourceAssemblerSupport<TaxiOrder, OrderResource> {
    public OrderResourceAssembler(){
        super(TaxiOrder.class, OrderResource.class);
    }

    @Override
    public OrderResource toResource(TaxiOrder taxiOrder) {

        OrderResource resource = new OrderResource();
        resource.setPrice(taxiOrder.getPrice());
        resource.setCarId(taxiOrder.getCar().getId());
        resource.setStatus(taxiOrder.getStatus());
        resource.setClientName(taxiOrder.getClientName());
        resource.setLocationCodeFrom(taxiOrder.getLocationCodeFrom());
        resource.setLocationCodeTo(taxiOrder.getLocationCodeTo());

        resource.add(linkTo(methodOn(OrdersController.class).getOrder(taxiOrder.getId())).withSelfRel());
        return resource;
    }
}
