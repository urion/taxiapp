package taxiapp.rest.resources.assembling;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import taxiapp.core.models.entities.Car;
import taxiapp.rest.mvc.TaxiesController;
import taxiapp.rest.resources.CarResource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class CarResourceAssembler extends ResourceAssemblerSupport<Car,CarResource> {
    public CarResourceAssembler(){
        super(Car.class,CarResource.class);
    }

    @Override
    public CarResource toResource(Car car) {

        CarResource resource = new CarResource();
        resource.setLocation(car.getLocation());
        resource.setDescription(car.getDescription());
        resource.setDriverName(car.getDriverName());
        resource.setStatus(car.getStatus());

        resource.add(linkTo(methodOn(TaxiesController.class).getCar(car.getId())).withSelfRel());
        return resource;
    }
}