package taxiapp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import taxiapp.core.services.exceptions.OtherExceptions.NoLocationsException;
import taxiapp.core.services.exceptions.OtherExceptions.NoRoadsException;
import taxiapp.core.services.interfaces.ILocationService;
import taxiapp.core.services.interfaces.IRoadService;
import taxiapp.core.services.util.LocationList;
import taxiapp.core.services.util.RoadList;
import taxiapp.rest.exceptions.NotFoundException;
import taxiapp.rest.resources.assembling.lists.LocationListResourceAssembler;
import taxiapp.rest.resources.assembling.lists.RoadListResourceAssembler;
import taxiapp.rest.resources.lists.LocationListResource;
import taxiapp.rest.resources.lists.RoadListResource;

/**
 * Created by urion on 23.11.15.
 */
@Controller
@RequestMapping("/general")
public class GeneralController {

    private ILocationService locationService;
    private IRoadService roadService;

    @Autowired
    public GeneralController(ILocationService locationService, IRoadService roadService) {
        this.locationService = locationService;
        this.roadService = roadService;
    }

    @RequestMapping(value = "/alllocations",
            method = RequestMethod.GET)
    public ResponseEntity<LocationListResource> getAllLocations() {
        try {
            //TODO throw proper exception
            LocationList locationsList = locationService.getAllLocations();
            LocationListResource locationListResource = new LocationListResourceAssembler().toResource(locationsList);
            return new ResponseEntity<>(locationListResource, HttpStatus.OK);
        } catch (NoLocationsException exception) {
            throw new NotFoundException(exception);
        }
    }

    @RequestMapping(value = "/allroads",
            method = RequestMethod.GET)
    public ResponseEntity<RoadListResource> getAllRoads() {
        try {
            //TODO throw proper exception
            RoadList roadList = roadService.getAllRoads();
            RoadListResource roadListResource = new RoadListResourceAssembler().toResource(roadList);
            return new ResponseEntity<>(roadListResource, HttpStatus.OK);
        } catch (NoRoadsException exception) {
            throw new NotFoundException(exception);
        }
    }
}
