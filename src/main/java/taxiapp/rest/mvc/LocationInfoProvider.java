package taxiapp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxiapp.core.services.interfaces.ILocationService;

/**
 * Created by urion on 31.01.16.
 */
@Component
public class LocationInfoProvider {

    @Autowired(required = true)
    public void setLocationService(ILocationService locationService) {
        LocationInfoProvider.locationService = locationService;
    }

    private static ILocationService locationService;

    public static ILocationService getLocationService() {
        return locationService;
    }
}
