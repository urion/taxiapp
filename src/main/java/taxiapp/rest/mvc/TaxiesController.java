package taxiapp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import taxiapp.core.models.entities.Car;
import taxiapp.core.models.entities.Location;
import taxiapp.core.models.entities.TaxiOrder;
import taxiapp.core.services.exceptions.CarExceptions.CarAlreadyExistsException;
import taxiapp.core.services.exceptions.CarExceptions.CarDoesNotExistException;
import taxiapp.core.services.exceptions.CarExceptions.CarIsAlreadyActiveException;
import taxiapp.core.services.exceptions.CarExceptions.CarIsNotBusyException;
import taxiapp.core.services.exceptions.OrderExceptions.OrderAlreadyActiveException;
import taxiapp.core.services.exceptions.OrderExceptions.OrderDoesNotExistException;
import taxiapp.core.services.exceptions.OtherExceptions.LocationDoesNotExistException;
import taxiapp.core.services.exceptions.OtherExceptions.NoOrdersException;
import taxiapp.core.services.interfaces.ICarService;
import taxiapp.core.services.interfaces.ILocationService;
import taxiapp.core.services.interfaces.IOrderService;
import taxiapp.core.services.util.OrdersList;
import taxiapp.rest.exceptions.BadRequestException;
import taxiapp.rest.exceptions.ConflictException;
import taxiapp.rest.exceptions.NotFoundException;
import taxiapp.rest.resources.CarResource;
import taxiapp.rest.resources.LocationResource;
import taxiapp.rest.resources.OrderResource;
import taxiapp.rest.resources.assembling.CarResourceAssembler;
import taxiapp.rest.resources.assembling.LocationResourceAssembler;
import taxiapp.rest.resources.assembling.OrderResourceAssembler;
import taxiapp.rest.resources.assembling.lists.OrdersListResourceAssembler;
import taxiapp.rest.resources.lists.OrdersListResource;

import java.net.URI;

@Controller
@RequestMapping("/taxies")
public class TaxiesController {
    private ICarService carService;
    private IOrderService orderService;
    private ILocationService locationService;

    @Autowired
    public TaxiesController(ICarService carService, IOrderService orderService, ILocationService locationService) {
        this.carService = carService;
        this.orderService = orderService;
        this.locationService = locationService;
    }

    @RequestMapping(value = "/{carId}",
            method = RequestMethod.GET)
    public ResponseEntity<CarResource> getCar(@PathVariable Long carId) {
        try {
            Car car = carService.findById(carId);

            CarResource result = new CarResourceAssembler().toResource(car);
            return new ResponseEntity<CarResource>(result, HttpStatus.OK);
        } catch (CarDoesNotExistException e) {
            throw new NotFoundException();
        }
    }
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CarResource> registerCar(@RequestBody CarResource carToRegister) {
        try {
            Car createdCar = carService.create(carToRegister.toCar());
            CarResource resource = new CarResourceAssembler().toResource(createdCar);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(resource.getLink("self").getHref()));
            return new ResponseEntity<>(resource, headers, HttpStatus.CREATED);
        } catch (CarAlreadyExistsException e) {
            throw new ConflictException(e);
        }
    }

    @RequestMapping(value = "/activatecar/{carId}",
            method = RequestMethod.POST)
    public ResponseEntity<CarResource> activateCar(@PathVariable Long carId, @RequestBody LocationResource location) {
        try {
            //TODO throw carDoesNotExistException from service layer
            //Car car = carService.findById(carId);
            locationService.isLocationCorrect(location.toLocation());
            Car responceCar = carService.activateCarWithLocation(carId, location.toLocation());

            CarResource result = new CarResourceAssembler().toResource(responceCar);
            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (CarDoesNotExistException | LocationDoesNotExistException e) {
            throw new NotFoundException(e);
        } catch (CarIsAlreadyActiveException e) {
            throw new ConflictException(e);
        }
    }

    @RequestMapping(value = "/orders",
            method = RequestMethod.GET)
    public ResponseEntity<OrdersListResource> getAllorders(@PathVariable Long accountId) {
        try {
            //TODO throw proper exception
            OrdersList ordersList = orderService.getAllServicesForDriver();

            OrdersListResource ordersListResource = new OrdersListResourceAssembler().toResource(ordersList);
            return new ResponseEntity<OrdersListResource>(ordersListResource, HttpStatus.OK);
        } catch (NoOrdersException e) {
            throw new NotFoundException(e);
        }
    }

    @RequestMapping(value = "acceptorder/{carId}/{orderId}",
            method = RequestMethod.PUT)
    public ResponseEntity<OrderResource> acceptOrder(
            @PathVariable Long carId, @PathVariable Long orderId) {
        try {
            //TODO exception throwing
            TaxiOrder taxiOrder = orderService.findById(orderId);
            Car car = carService.findById(carId);
            TaxiOrder responseTaxiOrder = orderService.acceptOrderByDriver(taxiOrder, car);

            OrderResource orderResource = new OrderResourceAssembler().toResource(responseTaxiOrder);
            return new ResponseEntity<OrderResource>(orderResource, HttpStatus.OK);
        } catch (OrderAlreadyActiveException e) {
            throw new ConflictException(e);
        } catch (OrderDoesNotExistException | CarDoesNotExistException e) {
            throw new NotFoundException(e);
        }
    }

    @RequestMapping(value = "/updatelocation/{carId}",
            method = RequestMethod.PUT)
    public ResponseEntity<LocationResource> updateLocation(
            @PathVariable Long carId, @RequestBody LocationResource location) {
        try {
            //TODO exception throwing
            locationService.isLocationCorrect(location.toLocation());
            Car car = carService.findById(carId);
            Location responceLocation = carService.updateCarLocation(carId, location.toLocation());

            LocationResource locationResource = new LocationResourceAssembler().toResource(responceLocation);
            return new ResponseEntity<LocationResource>(locationResource, HttpStatus.OK);
        } catch (LocationDoesNotExistException | CarDoesNotExistException e) {
            throw new NotFoundException(e);
        }
    }

    @RequestMapping(value = "/completeorder/{carId}",
            method = RequestMethod.PUT)
    public ResponseEntity<OrderResource> completeOrder(
            @PathVariable Long carId) {
        try {
            //TODO exception throwing
            //Car car = carService.findById(carId);
            TaxiOrder responseTaxiOrder = orderService.completeCarOrder(carId);

            OrderResource orderResource = new OrderResourceAssembler().toResource(responseTaxiOrder);
            return new ResponseEntity<OrderResource>(orderResource, HttpStatus.OK);
        } catch (CarDoesNotExistException e) {
            throw new NotFoundException(e);
        } catch (CarIsNotBusyException e) {
            throw new BadRequestException(e);
        }
    }

    @RequestMapping(value = "/stopwork/{carId}",
            method = RequestMethod.PUT)
    public ResponseEntity<CarResource> stopWork(
            @PathVariable Long carId) {
        try {
            //TODO exception throwing
            //Car car = carService.findById(carId);
            Car responseCar = carService.stopWork(carId);
            CarResource result = new CarResourceAssembler().toResource(responseCar);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (CarDoesNotExistException e) {
            throw new NotFoundException(e);
        } catch (CarIsNotBusyException e) {
            throw new BadRequestException(e);
        }
    }


}
