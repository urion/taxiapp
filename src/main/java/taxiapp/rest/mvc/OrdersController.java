package taxiapp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import taxiapp.core.models.entities.TaxiOrder;
import taxiapp.core.services.exceptions.OrderExceptions.*;
import taxiapp.core.services.interfaces.IOrderService;
import taxiapp.rest.exceptions.BadRequestException;
import taxiapp.rest.exceptions.ConflictException;
import taxiapp.rest.exceptions.NotFoundException;
import taxiapp.rest.resources.OrderResource;
import taxiapp.rest.resources.assembling.OrderResourceAssembler;

import java.net.URI;

/**
 * Created by urion on 25.10.15.
 */
@Controller
@RequestMapping("/orders")
public class OrdersController {

    private IOrderService orderService;

    @Autowired
    public OrdersController(IOrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<OrderResource> createOrder(@RequestBody OrderResource sentOrder) {
        try {
            TaxiOrder createdTaxiOrder = orderService.create(sentOrder.toOrder());
            OrderResource resourse = new OrderResourceAssembler().toResource(createdTaxiOrder);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(resourse.getLink("self").getHref()));
            return new ResponseEntity<>(resourse, headers, HttpStatus.CREATED);
        } catch (OrderAlreadyExistsException exception) {
            throw new ConflictException();
        }
    }

    @RequestMapping(value = "/acceptorder/{orderId}",
            method = RequestMethod.GET)
    public ResponseEntity<OrderResource> acceptOrder(@PathVariable Long orderId) {

        try {
            TaxiOrder taxiOrder = orderService.findById(orderId);
            TaxiOrder responseTaxiOrder = orderService.acceptOrderByClient(taxiOrder);

            OrderResource result = new OrderResourceAssembler().toResource(responseTaxiOrder);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (OrderDoesNotExistException e) {
            throw new NotFoundException();
        } catch (OrderAlreadyRejectedException | OrderAlreadyActiveException e) {
            throw new BadRequestException();
        }
    }

    @RequestMapping(value = "/rejectorder/{orderId}",
            method = RequestMethod.GET)
    public ResponseEntity<OrderResource> rejectOrder(@PathVariable Long orderId) {
        try {
            TaxiOrder taxiOrder = orderService.findById(orderId);
            TaxiOrder responseTaxiOrder = orderService.rejectOrderByClient(taxiOrder);

            OrderResource result = new OrderResourceAssembler().toResource(responseTaxiOrder);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (OrderDoesNotExistException e) {
            throw new NotFoundException();
        } catch (OrderAlreadyRejectedException | OrderAlreadyActiveException | OrderCannotBeRejectedException e) {
            throw new BadRequestException();
        }
    }
    @RequestMapping(value = "/{orderId}",
            method = RequestMethod.GET)
    public ResponseEntity<OrderResource> getOrder(@PathVariable Long orderId) {
        TaxiOrder taxiOrder = orderService.findById(orderId);
        if (taxiOrder != null) {
            OrderResource res = new OrderResourceAssembler().toResource(taxiOrder);
            return new ResponseEntity<OrderResource>(res, HttpStatus.OK);
        } else {
            return new ResponseEntity<OrderResource>(HttpStatus.NOT_FOUND);
        }
    }

}
