package taxiapp.core.services.util;

import taxiapp.core.models.entities.TaxiOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by urion on 22.11.15.
 */
public class OrdersList {
    private List<TaxiOrder> taxiOrders = new ArrayList<>();

    public OrdersList(List<TaxiOrder> taxiOrders) {
        this.taxiOrders = taxiOrders;
    }

    public List<TaxiOrder> getTaxiOrders() {
        return taxiOrders;
    }

    public void setTaxiOrders(List<TaxiOrder> taxiOrders) {
        this.taxiOrders = taxiOrders;
    }
}
