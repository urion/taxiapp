package taxiapp.core.services.util;

import taxiapp.core.models.entities.Car;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by urion on 23.11.15.
 */
public class CarList {
    public CarList(List<Car> cars) {
        this.cars = cars;
    }

    private List<Car> cars = new ArrayList<>();

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
