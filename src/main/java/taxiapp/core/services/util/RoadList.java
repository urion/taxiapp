package taxiapp.core.services.util;

import taxiapp.core.models.entities.Road;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by urion on 23.11.15.
 */
public class RoadList {
    private List<Road> roads = new ArrayList<>();

    public RoadList(List<Road> roads) {
        this.roads = roads;
    }

    public List<Road> getRoads() {
        return roads;
    }

    public void setRoads(List<Road> roads) {
        this.roads = roads;
    }
}
