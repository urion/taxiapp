package taxiapp.core.services.util;

import taxiapp.core.models.entities.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by urion on 23.11.15.
 */
public class LocationList {
    private List<Location> locations = new ArrayList<>();

    public LocationList(List<Location> locations) {
        this.locations = locations;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
