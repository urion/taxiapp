package taxiapp.core.services.exceptions.CarExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class CarIsAlreadyActiveException extends RuntimeException {
    public CarIsAlreadyActiveException(Throwable cause) {
        super(cause);
    }

    public CarIsAlreadyActiveException(String message, Throwable cause) {
        super(message, cause);
    }

    public CarIsAlreadyActiveException(String message) {
        super(message);
    }

    public CarIsAlreadyActiveException() {
    }
}
