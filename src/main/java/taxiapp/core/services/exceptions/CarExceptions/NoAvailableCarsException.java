package taxiapp.core.services.exceptions.CarExceptions;

/**
 * Created by urion on 25.10.15.
 */
public class NoAvailableCarsException extends RuntimeException {
    public NoAvailableCarsException(Throwable cause) {
        super(cause);
    }
    public NoAvailableCarsException (String message, Throwable cause) {
        super(message, cause);
    }
    public NoAvailableCarsException (String message) {
        super(message);
    }
    public NoAvailableCarsException () {
    }
}
