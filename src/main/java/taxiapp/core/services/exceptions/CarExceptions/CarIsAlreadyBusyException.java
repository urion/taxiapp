package taxiapp.core.services.exceptions.CarExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class CarIsAlreadyBusyException extends RuntimeException {
    public CarIsAlreadyBusyException(Throwable cause) {
        super(cause);
    }

    public CarIsAlreadyBusyException(String message, Throwable cause) {
        super(message, cause);
    }

    public CarIsAlreadyBusyException(String message) {
        super(message);
    }

    public CarIsAlreadyBusyException() {
    }
}
