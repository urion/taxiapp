package taxiapp.core.services.exceptions.CarExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class CarDoesNotExistException extends RuntimeException {
    public CarDoesNotExistException(Throwable cause) {
        super(cause);
    }

    public CarDoesNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public CarDoesNotExistException(String message) {
        super(message);
    }

    public CarDoesNotExistException() {
    }
}
