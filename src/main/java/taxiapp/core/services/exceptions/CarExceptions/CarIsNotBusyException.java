package taxiapp.core.services.exceptions.CarExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class CarIsNotBusyException extends RuntimeException {
    public CarIsNotBusyException(Throwable cause) {
        super(cause);
    }

    public CarIsNotBusyException(String message, Throwable cause) {
        super(message, cause);
    }

    public CarIsNotBusyException(String message) {
        super(message);
    }

    public CarIsNotBusyException() {
    }

}
