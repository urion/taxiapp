package taxiapp.core.services.exceptions.CarExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class CarAlreadyExistsException extends RuntimeException {
    public CarAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public CarAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public CarAlreadyExistsException(String message) {
        super(message);
    }

    public CarAlreadyExistsException() {
    }
}
