package taxiapp.core.services.exceptions.OrderExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class OrderAlreadyRejectedException extends RuntimeException {
    public OrderAlreadyRejectedException(Throwable cause) {
        super(cause);
    }

    public OrderAlreadyRejectedException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrderAlreadyRejectedException(String message) {
        super(message);
    }

    public OrderAlreadyRejectedException() {
    }
}
