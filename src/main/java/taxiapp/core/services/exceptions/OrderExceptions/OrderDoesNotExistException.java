package taxiapp.core.services.exceptions.OrderExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class OrderDoesNotExistException extends RuntimeException {
    public OrderDoesNotExistException(Throwable cause) {
        super(cause);
    }

    public OrderDoesNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrderDoesNotExistException(String message) {
        super(message);
    }

    public OrderDoesNotExistException() {
    }
}
