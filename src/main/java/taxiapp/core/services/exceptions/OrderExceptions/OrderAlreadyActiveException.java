package taxiapp.core.services.exceptions.OrderExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class OrderAlreadyActiveException extends RuntimeException {
    public OrderAlreadyActiveException(Throwable cause) {
        super(cause);
    }

    public OrderAlreadyActiveException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrderAlreadyActiveException(String message) {
        super(message);
    }

    public OrderAlreadyActiveException() {
    }
}
