package taxiapp.core.services.exceptions.OrderExceptions;

/**
 * Created by urion on 29.10.15.
 */
public class OrderAlreadyExistsException extends RuntimeException {
    public OrderAlreadyExistsException(Throwable cause) {
        super(cause);
    }
    public OrderAlreadyExistsException (String message, Throwable cause) {
        super(message, cause);
    }
    public OrderAlreadyExistsException (String message) {
        super(message);
    }
    public OrderAlreadyExistsException () {
    }
}
