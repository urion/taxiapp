package taxiapp.core.services.exceptions.OrderExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class OrderCannotBeRejectedException extends RuntimeException {
    public OrderCannotBeRejectedException(Throwable cause) {
        super(cause);
    }

    public OrderCannotBeRejectedException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrderCannotBeRejectedException(String message) {
        super(message);
    }

    public OrderCannotBeRejectedException() {
    }
}
