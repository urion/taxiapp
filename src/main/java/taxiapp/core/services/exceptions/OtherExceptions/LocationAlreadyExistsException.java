package taxiapp.core.services.exceptions.OtherExceptions;

/**
 * Created by urion on 01.12.15.
 */
public class LocationAlreadyExistsException extends RuntimeException {
    public LocationAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public LocationAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public LocationAlreadyExistsException(String message) {
        super(message);
    }

    public LocationAlreadyExistsException() {
    }
}
