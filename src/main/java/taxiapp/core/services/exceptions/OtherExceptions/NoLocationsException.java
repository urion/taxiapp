package taxiapp.core.services.exceptions.OtherExceptions;

/**
 * Created by urion on 30.11.15.
 */
public class NoLocationsException extends RuntimeException {
    public NoLocationsException(Throwable cause) {
        super(cause);
    }

    public NoLocationsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoLocationsException(String message) {
        super(message);
    }

    public NoLocationsException() {
    }
}
