package taxiapp.core.services.exceptions.OtherExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class NoOrdersException extends RuntimeException {
    public NoOrdersException(Throwable cause) {
        super(cause);
    }

    public NoOrdersException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoOrdersException(String message) {
        super(message);
    }

    public NoOrdersException() {
    }
}
