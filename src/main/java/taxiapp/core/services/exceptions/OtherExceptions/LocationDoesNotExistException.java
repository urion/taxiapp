package taxiapp.core.services.exceptions.OtherExceptions;

/**
 * Created by urion on 22.11.15.
 */
public class LocationDoesNotExistException extends RuntimeException {
    public LocationDoesNotExistException(Throwable cause) {
        super(cause);
    }

    public LocationDoesNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public LocationDoesNotExistException(String message) {
        super(message);
    }

    public LocationDoesNotExistException() {
    }
}
