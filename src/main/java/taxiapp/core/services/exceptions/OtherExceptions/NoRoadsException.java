package taxiapp.core.services.exceptions.OtherExceptions;

/**
 * Created by urion on 30.11.15.
 */
public class NoRoadsException extends RuntimeException {
    public NoRoadsException(Throwable cause) {
        super(cause);
    }

    public NoRoadsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoRoadsException(String message) {
        super(message);
    }

    public NoRoadsException() {
    }
}

