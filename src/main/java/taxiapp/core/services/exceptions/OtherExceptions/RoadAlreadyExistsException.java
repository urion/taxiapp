package taxiapp.core.services.exceptions.OtherExceptions;

/**
 * Created by urion on 01.12.15.
 */
public class RoadAlreadyExistsException extends RuntimeException {
    public RoadAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public RoadAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoadAlreadyExistsException(String message) {
        super(message);
    }

    public RoadAlreadyExistsException() {
    }
}
