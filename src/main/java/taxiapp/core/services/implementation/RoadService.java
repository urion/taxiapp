package taxiapp.core.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import taxiapp.core.DAL.implementation.RoadRepository;
import taxiapp.core.models.entities.Road;
import taxiapp.core.services.exceptions.CarExceptions.CarDoesNotExistException;
import taxiapp.core.services.exceptions.OtherExceptions.NoRoadsException;
import taxiapp.core.services.exceptions.OtherExceptions.RoadAlreadyExistsException;
import taxiapp.core.services.interfaces.IRoadService;
import taxiapp.core.services.util.RoadList;

import java.util.List;

/**
 * Created by urion on 01.12.15.
 */
@Service
@Transactional
public class RoadService implements IRoadService {
    @Autowired
    RoadRepository roadRepository;

    @Override
    public RoadList getAllRoads() {
        RoadList roadList = new RoadList(roadRepository.findAll());
        if (roadList.getRoads().isEmpty()) throw new NoRoadsException();
        return roadList;
    }


    public Road create(Road entity) {
        List<Road> roadList = roadRepository.findByPredicate(roadInCollection ->
                roadInCollection.getStart() == entity.getStart()
                        && roadInCollection.getFinish() == entity.getFinish());
        if (!roadList.isEmpty())
            throw new RoadAlreadyExistsException("Road with such start and finish already exists!");
        return roadRepository.create(entity);
    }


    public Road findById(Long id) {
        Road road = roadRepository.findById(id);
        if (road == null)
            throw new CarDoesNotExistException("There is no road with Id= " + id);
        return road;
    }
}
