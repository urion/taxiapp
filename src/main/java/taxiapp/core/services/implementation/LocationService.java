package taxiapp.core.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import taxiapp.core.DAL.implementation.LocationRepository;
import taxiapp.core.models.entities.Location;
import taxiapp.core.services.exceptions.CarExceptions.CarDoesNotExistException;
import taxiapp.core.services.exceptions.OtherExceptions.LocationAlreadyExistsException;
import taxiapp.core.services.exceptions.OtherExceptions.LocationDoesNotExistException;
import taxiapp.core.services.exceptions.OtherExceptions.NoLocationsException;
import taxiapp.core.services.interfaces.ILocationService;
import taxiapp.core.services.util.LocationList;

import java.util.List;

/**
 * Created by urion on 01.12.15.
 */
@Service
@Transactional
public class LocationService implements ILocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public boolean isLocationCorrect(Location location) {
        List<Location> foundLocationList = locationRepository.findByPredicate(locationInCollection ->
                locationInCollection.getCode() == location.getCode());
        if (foundLocationList.isEmpty())
            throw new LocationDoesNotExistException("Location with such code does not exists");
        return true;
    }

    @Override
    public LocationList getAllLocations() {
        LocationList locationList = new LocationList(locationRepository.findAll());
        if (locationList.getLocations().isEmpty()) throw new NoLocationsException();
        return locationList;
    }

    @Override
    public Location getLocationByLocationCode(int code) {
        List<Location> locations = locationRepository.findByPredicate(location -> location.getCode() == code);
        if (locations.isEmpty()) throw new NoLocationsException();
        System.out.print(")))))))))))))))))))))))))))))))))))))))))\n))))))))))))))))\n))))))))))))");
        for (Location l : locations) {
            System.out.print(l.toString());
        }
        return locations.stream().findFirst().get();
    }


    public Location create(Location entity) {
        List<Location> location = locationRepository.findByPredicate(locationInCollection ->
                locationInCollection.getCode() == entity.getCode());
        if (!location.isEmpty())
            throw new LocationAlreadyExistsException("Location with such code already exists!");
        return locationRepository.create(entity);
    }


    public Location findById(Long id) {
        Location location = locationRepository.findById(id);
        if (location == null)
            throw new CarDoesNotExistException("There is no location with Id= " + id);
        return location;
    }
}
