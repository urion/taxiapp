package taxiapp.core.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import taxiapp.core.DAL.implementation.CarRepository;
import taxiapp.core.DAL.implementation.OrderRepository;
import taxiapp.core.models.additional.CarStatus;
import taxiapp.core.models.additional.OrderStatus;
import taxiapp.core.models.entities.Car;
import taxiapp.core.models.entities.TaxiOrder;
import taxiapp.core.models.logic.interfaces.IOrdersProcessor;
import taxiapp.core.services.exceptions.OrderExceptions.OrderAlreadyActiveException;
import taxiapp.core.services.exceptions.OrderExceptions.OrderAlreadyExistsException;
import taxiapp.core.services.exceptions.OrderExceptions.OrderCannotBeRejectedException;
import taxiapp.core.services.exceptions.OrderExceptions.OrderDoesNotExistException;
import taxiapp.core.services.exceptions.OtherExceptions.NoOrdersException;
import taxiapp.core.services.interfaces.IOrderService;
import taxiapp.core.services.util.OrdersList;

import java.util.List;

/**
 * Created by urion on 01.12.15.
 */
@Service
@Transactional
public class OrderService implements IOrderService {
    @Autowired
    CarRepository carRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    IOrdersProcessor ordersProcessor;

    @Override
    public OrdersList getAllServicesForDriver() {
        List<TaxiOrder> waitingOrdersList = orderRepository.findByPredicate(
                entity -> entity.getStatus() == OrderStatus.toInt(OrderStatus.Waiting));

        if (waitingOrdersList.isEmpty())
            throw new NoOrdersException("There are no waiting orders");

        return new OrdersList(waitingOrdersList);
    }

    @Override
    public TaxiOrder acceptOrderByDriver(TaxiOrder taxiOrder, Car car) {
        //TODO check if ids are correct
        Car carToAcceptOrder = carRepository.findById(car.getId());
        carToAcceptOrder.setStatus(CarStatus.toInt(CarStatus.Busy));
        carRepository.update(car.getId(), carToAcceptOrder);

        TaxiOrder taxiOrderToAccept = findById(taxiOrder.getId());

        if (taxiOrderToAccept.getStatus() != OrderStatus.toInt(OrderStatus.Waiting))
            throw new OrderAlreadyActiveException();

        taxiOrderToAccept.setStatus(OrderStatus.toInt(OrderStatus.InProcess));
        orderRepository.update(taxiOrder.getId(), taxiOrderToAccept);
        return taxiOrderToAccept;
    }

    @Override
    public TaxiOrder acceptOrderByClient(TaxiOrder taxiOrder) {
        taxiOrder.setStatus(OrderStatus.toInt(OrderStatus.Waiting));
        orderRepository.create(taxiOrder);
        return taxiOrder;
    }

    @Override
    public TaxiOrder rejectOrderByClient(TaxiOrder taxiOrder) {
        //TODO check
        TaxiOrder taxiOrderToReject = orderRepository.findById(taxiOrder.getId());

        if (taxiOrderToReject == null)
            return taxiOrder;

        if (taxiOrderToReject.getStatus() == OrderStatus.toInt(OrderStatus.Waiting)) {
            orderRepository.delete(taxiOrderToReject);
            return taxiOrderToReject;
        } else {
            throw new OrderCannotBeRejectedException();
        }
    }

    @Override
    public TaxiOrder create(TaxiOrder entity) {
        if (entity.getId() != null) {
            TaxiOrder taxiOrder = orderRepository.findById(entity.getId());
            if (taxiOrder != null)
                throw new OrderAlreadyExistsException();
        }
        return ordersProcessor.addSystemInfoForOrder(entity);
    }

    @Override
    public TaxiOrder findById(Long id) {
        TaxiOrder taxiOrder = orderRepository.findById(id);
        if (taxiOrder == null)
            throw new OrderDoesNotExistException("There is no taxiOrder with Id= " + id);
        return taxiOrder;
    }

    @Override
    public TaxiOrder completeCarOrder(long carId) {

        Car carToCompleteOrder = carRepository.findById(carId);
        carToCompleteOrder.setStatus(CarStatus.toInt(CarStatus.Available));

        TaxiOrder taxiOrderToComplete = orderRepository.findByPredicate(
                order -> order.getCar().getId() == carId)
                .stream()
                .findFirst()
                .get();

        taxiOrderToComplete.setStatus(OrderStatus.toInt(OrderStatus.Completed));
        orderRepository.update(taxiOrderToComplete.getId(), taxiOrderToComplete);
        return taxiOrderToComplete;
    }
}
