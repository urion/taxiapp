package taxiapp.core.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import taxiapp.core.DAL.implementation.CarRepository;
import taxiapp.core.DAL.implementation.LocationRepository;
import taxiapp.core.models.additional.CarStatus;
import taxiapp.core.models.entities.Car;
import taxiapp.core.models.entities.Location;
import taxiapp.core.services.exceptions.CarExceptions.CarAlreadyExistsException;
import taxiapp.core.services.exceptions.CarExceptions.CarDoesNotExistException;
import taxiapp.core.services.exceptions.CarExceptions.CarIsAlreadyActiveException;
import taxiapp.core.services.exceptions.OtherExceptions.LocationDoesNotExistException;
import taxiapp.core.services.interfaces.ICarService;

import java.util.List;

/**
 * Created by urion on 01.12.15.
 */
@Service
@Transactional
public class CarService implements ICarService {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private LocationRepository locationRepository;

    @Override
    public Car activateCarWithLocation(long id, Location location) {
        Car carToActivate = findById(id);
        CarStatus carStat = CarStatus.fromInt(carToActivate.getStatus());

        if (carStat == CarStatus.Busy || carStat == CarStatus.Available)
            throw new CarIsAlreadyActiveException("Car with Id= " + id + " is already Active!");

        carToActivate.setLocation(location);
        carToActivate.setStatus(CarStatus.toInt(CarStatus.Available));
        return carRepository.update(id, carToActivate);
    }


    @Override
    public Car create(Car entity) {
        List<Car> carList = carRepository.findByPredicate(carInCollection ->
                carInCollection.getDriverName() == entity.getDriverName() &&
                        carInCollection.getDescription() == entity.getDescription());
        if (!carList.isEmpty())
            throw new CarAlreadyExistsException("Car with such description and driver already exists");
        return carRepository.create(entity);
    }

    @Override
    public Car findById(Long id) {
        Car car = carRepository.findById(id);
        if (car == null)
            throw new CarDoesNotExistException("There is no car with Id= " + id);
        return car;
    }

    @Override
    public Location updateCarLocation(long id, Location location) {
        Car carToLocate = findById(id);
        List<Location> foundLocationList = locationRepository.findByPredicate(e -> e.getCode() == location.getCode());
        if (foundLocationList.isEmpty())
            throw new LocationDoesNotExistException("Location you want to set does not exist");
        carToLocate.setLocation(location);
        return carRepository.update(id, carToLocate).getLocation();
    }

    @Override
    public Car stopWork(long id) {
        Car carToStopWork = findById(id);
        carToStopWork.setStatus(CarStatus.toInt(CarStatus.Inactive));
        return carRepository.update(id, carToStopWork);
    }
}
