package taxiapp.core.services.interfaces;

import taxiapp.core.models.entities.Location;
import taxiapp.core.services.util.LocationList;

/**
 * Created by urion on 25.10.15.
 */
public interface ILocationService extends IGenericService<Location> {
    // TODO throw appropriate exception from carSevice if location doesn't exist
    boolean isLocationCorrect(Location location);
    LocationList getAllLocations();

    Location getLocationByLocationCode(int code);
}
