package taxiapp.core.services.interfaces;

/**
 * Created by urion on 25.10.15.
 */
public interface IGenericService<T> {
    T create(T entity);
    T findById(Long id);
}
