package taxiapp.core.services.interfaces;

import taxiapp.core.models.entities.Road;
import taxiapp.core.services.util.RoadList;

/**
 * Created by urion on 25.10.15.
 */
public interface IRoadService {//} extends IGenericService<Road> {
    RoadList getAllRoads();

    Road create(Road entity);
}
