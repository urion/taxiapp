package taxiapp.core.services.interfaces;

import taxiapp.core.models.entities.Car;
import taxiapp.core.models.entities.TaxiOrder;
import taxiapp.core.services.util.OrdersList;

/**
 * Created by urion on 25.10.15.
 */
public interface IOrderService extends IGenericService<TaxiOrder> {
    OrdersList getAllServicesForDriver();

    TaxiOrder acceptOrderByDriver(TaxiOrder taxiOrder, Car car);

    TaxiOrder acceptOrderByClient(TaxiOrder taxiOrder);

    TaxiOrder rejectOrderByClient(TaxiOrder taxiOrder);

    TaxiOrder completeCarOrder(long carId);
}
