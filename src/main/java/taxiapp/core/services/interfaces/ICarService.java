package taxiapp.core.services.interfaces;

import taxiapp.core.models.entities.Car;
import taxiapp.core.models.entities.Location;

/**
 * Created by urion on 25.10.15.
 */
public interface ICarService extends IGenericService<Car> {
     Car activateCarWithLocation(long id, Location location);

     Location updateCarLocation(long id, Location location);

     Car stopWork(long id);
}
