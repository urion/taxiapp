package taxiapp.core.services.initializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxiapp.core.models.entities.Car;
import taxiapp.core.models.entities.Location;
import taxiapp.core.models.entities.Road;
import taxiapp.core.models.entities.TaxiOrder;
import taxiapp.core.services.interfaces.ICarService;
import taxiapp.core.services.interfaces.ILocationService;
import taxiapp.core.services.interfaces.IOrderService;
import taxiapp.core.services.interfaces.IRoadService;
import taxiapp.core.services.util.LocationList;

import java.util.List;

/**
 * Created by urion on 13.12.15.
 */
@Component
public class DataBaseInitializer {

    ILocationService locationService;
    IRoadService roadService;
    ICarService carService;
    IOrderService orderService;

    @Autowired
    public DataBaseInitializer(ILocationService locationService, IRoadService roadService, ICarService carService, IOrderService orderService) {
        this.locationService = locationService;
        this.roadService = roadService;
        this.carService = carService;
        this.orderService = orderService;

        initializeBasicData();
    }
    private void initializeBasicData() {
        initializeLocations();
        initializeRoads();
        initializeCars();
        initializeOrders();
    }

    //Car tempCar;
    private void initializeCars() {

        LocationList list = locationService.getAllLocations();
        List<Location> locations = list.getLocations();

        Car car1 = new Car();
        //car1.setId(1L);
        car1.setDescription("Yellow Jigul");
        car1.setDriverName("Vovan");
        car1.setLocation(locations.get(0));
        car1.setStatus(0);

        Car car2 = new Car();
        //car2.setId(2L);
        car2.setDescription("Black Passat");
        car2.setDriverName("Paul");
        car2.setLocation(locations.get(2));
        car2.setStatus(2);

        Car car01 = carService.create(car1);
        Car car02 = carService.create(car2);
        System.out.print("\n\n\n\n\n\n\n\n\n\n id1 : " + car01.getId() +
                "\n + id2 : " + car02.getId() + "\n\n\n\n\n\n\n\n\n");
        // tempCar = car2;
    }

    private void initializeOrders() {

        TaxiOrder order1 = new TaxiOrder();
        //Car car = carRepository.findById(1L);
        //order1.setCar(tempCar);
        order1.setClientName("Jeremy");
        //order1.setId(1L);
        order1.setLocationCodeFrom(0);
        order1.setLocationCodeTo(2);
        order1.setPrice(29.0);
        order1.setStatus(0);
//        Path path = new Path();
//        order1.setPath(path);
//        System.out.println("\n\n\n\n\n\n\n   "+order1.getLocationCodeTo()+"\n\n\n\n\n\n   ");
//        System.out.println("\n\n\n\n\n\n   "+order1.getId()+"\n\n\n\n\n\n\n\n   ");
//        orderRepository.create(order1);
    }

    private void initializeRoads() {
        LocationList list = locationService.getAllLocations();
        List<Location> locations = list.getLocations();
        Road road1 = new Road();
        road1.setStart(locations.get(0));
        road1.setFinish(locations.get(1));
        road1.setLength(10.0);
        Road road2 = new Road();
        road2.setStart(locations.get(0));
        road2.setFinish(locations.get(2));
        road2.setLength(7.0);
        Road road3 = new Road();
        road3.setStart(locations.get(1));
        road3.setFinish(locations.get(2));
        road3.setLength(3.0);
        Road road4 = new Road();
        road4.setStart(locations.get(1));
        road4.setFinish(locations.get(3));
        road4.setLength(8.0);
        Road road5 = new Road();
        road5.setStart(locations.get(2));
        road5.setFinish(locations.get(4));
        road5.setLength(10.0);

        roadService.create(road2);
        roadService.create(road3);
        roadService.create(road4);
        roadService.create(road5);
    }

    private void initializeLocations() {
        for (int i = 1; i <= 5; i++) {
            Location location = new Location();
            location.setCode(i);
            locationService.create(location);
        }
        LocationList list = locationService.getAllLocations();
        List<Location> locations = list.getLocations();
    }
}
