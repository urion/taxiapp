package taxiapp.core.DAL.implementation;

import org.springframework.stereotype.Repository;
import taxiapp.core.DAL.generic.GenericRepository;
import taxiapp.core.models.entities.Location;

import javax.persistence.Query;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by urion on 22.11.15.
 */
@Repository
public class LocationRepository extends GenericRepository<Location> {
    @Override
    public Location update(long id, Location entity) {
        Location location = em.find(Location.class, id);
        location.setCode(entity.getCode());
        return location;
    }

    @Override
    public List<Location> findByPredicate(Predicate<Location> predicate) {
        Query query = em.createQuery("SELECT l FROM Location l");
        return (List<Location>) query
                .getResultList()
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public List<Location> findAll() {
        Query query = em.createQuery("SELECT l FROM Location l");
        return query.getResultList();
    }

    @Override
    protected Class<Location> getPersistentClass() {
        return Location.class;
    }
}
