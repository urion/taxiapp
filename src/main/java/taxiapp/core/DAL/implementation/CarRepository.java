package taxiapp.core.DAL.implementation;

import org.springframework.stereotype.Repository;
import taxiapp.core.DAL.generic.GenericRepository;
import taxiapp.core.models.entities.Car;

import javax.persistence.Query;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by urion on 22.11.15.
 */
@Repository
public class CarRepository extends GenericRepository<Car> {
    @Override
    public Car update(long id, Car entity) {
        Car car = em.find(Car.class, id);
        car.setDescription(entity.getDescription());
        car.setLocation(entity.getLocation());
        car.setDriverName(entity.getDriverName());
        return car;
    }

    @Override
    public List<Car> findByPredicate(Predicate<Car> predicate) {
        Query query = em.createQuery("SELECT c FROM Car c");
        return (List<Car>) query.getResultList()
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public List<Car> findAll() {
        Query query = em.createQuery("SELECT c FROM Car c");
        return query.getResultList();
    }

    @Override
    protected Class<Car> getPersistentClass() {
        return Car.class;
    }
}
