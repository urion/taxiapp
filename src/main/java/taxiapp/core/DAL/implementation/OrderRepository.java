package taxiapp.core.DAL.implementation;

import org.springframework.stereotype.Repository;
import taxiapp.core.DAL.generic.GenericRepository;
import taxiapp.core.models.entities.TaxiOrder;

import javax.persistence.Query;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by urion on 02.12.15.
 */
@Repository
public class OrderRepository extends GenericRepository<TaxiOrder> {
    @Override
    public TaxiOrder update(long id, TaxiOrder entity) {
        TaxiOrder taxiOrderToUpdate = em.find(TaxiOrder.class, id);
        taxiOrderToUpdate.setPrice(entity.getPrice());
        taxiOrderToUpdate.setStatus(entity.getStatus());
        taxiOrderToUpdate.setCar(entity.getCar());
        taxiOrderToUpdate.setClientName(entity.getClientName());
        taxiOrderToUpdate.setPath(entity.getPath());
        return taxiOrderToUpdate;
    }

    @Override
    public List<TaxiOrder> findByPredicate(Predicate<TaxiOrder> predicate) {
        Query query = em.createQuery("SELECT o FROM TaxiOrder o");
        return (List<TaxiOrder>) query
                .getResultList()
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public List<TaxiOrder> findAll() {
        Query query = em.createQuery("SELECT o FROM TaxiOrder o");
        return query.getResultList();
    }

    @Override
    protected Class<TaxiOrder> getPersistentClass() {
        return TaxiOrder.class;
    }
}
