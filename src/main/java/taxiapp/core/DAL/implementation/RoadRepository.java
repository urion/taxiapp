package taxiapp.core.DAL.implementation;

import org.springframework.stereotype.Repository;
import taxiapp.core.DAL.generic.GenericRepository;
import taxiapp.core.models.entities.Road;

import javax.persistence.Query;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by urion on 22.11.15.
 */
@Repository
public class RoadRepository extends GenericRepository<Road> {
    @Override
    public Road update(long id, Road entity) {
        Road road = em.find(Road.class, id);
        road.setStart(entity.getStart());
        road.setFinish(entity.getFinish());
        return road;
    }

    @Override
    public List<Road> findByPredicate(Predicate<Road> predicate) {
        Query query = em.createQuery("SELECT r FROM Road r");
        return (List<Road>) query
                .getResultList()
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public List<Road> findAll() {
        Query query = em.createQuery("SELECT r FROM Road r");
        return query.getResultList();
    }

    @Override
    protected Class<Road> getPersistentClass() {
        return Road.class;
    }
}
