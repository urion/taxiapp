package taxiapp.core.DAL.generic;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by urion on 21.11.15.
 */
@Repository
public abstract class GenericRepository<T> {

    @PersistenceContext
    protected EntityManager em;

    public T create(T entity) {
        em.persist(entity);
        return entity;
    }

    public T delete(T entity) {
        em.remove(entity);
        return entity;
    }

    public T delete(long id) {
        T entity = findById(id);
        return delete(entity);
    }

    public T findById(long id) {
        return em.find(this.getPersistentClass(), id);
    }

    public abstract T update(long id, T entity);

    public abstract List<T> findByPredicate(Predicate<T> predicate);

    public abstract List<T> findAll();

    protected abstract Class<T> getPersistentClass();
}
