package taxiapp.core.models.logic.interfaces;

import taxiapp.core.models.entities.Path;

/**
 * Created by urion on 02.12.15.
 */
public interface IPathFinder {
    Path findPath(int locationCodeFrom, int locationCodeTo);
}
