package taxiapp.core.models.logic.interfaces;

import taxiapp.core.models.entities.Road;

/**
 * Created by urion on 02.12.15.
 */
public interface IMap {
    double getLength(int from, int to);

    Road getRoad(int from, int to);

    double INF();

    int N();
}
