package taxiapp.core.models.logic.interfaces;

import taxiapp.core.models.entities.TaxiOrder;

/**
 * Created by urion on 02.12.15.
 */
public interface IOrdersProcessor {
    TaxiOrder addSystemInfoForOrder(TaxiOrder taxiOrderFromClient);
}
