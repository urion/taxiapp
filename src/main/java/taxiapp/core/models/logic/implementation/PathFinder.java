package taxiapp.core.models.logic.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxiapp.core.models.entities.Path;
import taxiapp.core.models.entities.Road;
import taxiapp.core.models.logic.interfaces.IMap;
import taxiapp.core.models.logic.interfaces.IPathFinder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by urion on 27.10.15.
 */
@Component
public class PathFinder implements IPathFinder {
    IMap map;

    @Autowired
    public PathFinder(IMap map) {
        this.map = map;
    }

    @Override
    public Path findPath(int locationCodeFrom, int locationCodeTo) {

        //Initialization
        ArrayList<Double> distances = new ArrayList<>();
        ArrayList<Integer> cameFrom = new ArrayList<>();
        ArrayList<Boolean> covered = new ArrayList<>();
        for (int i = 0; i < map.N(); i++) {
            distances.add(map.INF());
            cameFrom.add(-1);
            covered.add(false);
        }
        distances.set(locationCodeFrom, 0.0);

        //Main loop
        for (; ; ) {
            int chosenVertex = -1;
            for (int candidateVertex = 0; candidateVertex < map.N(); candidateVertex++) {
                if (!covered.get(candidateVertex)
                        && distances.get(candidateVertex) < map.INF()
                        && (chosenVertex == -1 || distances.get(candidateVertex) < distances.get(chosenVertex))) {
                    chosenVertex = candidateVertex;
                }
            }
            if (chosenVertex == -1)
                break;
            covered.set(chosenVertex, true);

            for (int v = 0; v < map.N(); v++) {
                double pathLength = map.getLength(chosenVertex, v);
                if (!covered.get(chosenVertex)
                        && pathLength < map.INF()) {

                    distances.set(v, Math.min(
                            distances.get(v),
                            distances.get(chosenVertex) + pathLength));

                    cameFrom.set(v, chosenVertex);
                }
            }
        }

        //Shortest path reconstruction
        List<Road> path = new LinkedList<>();
        int previousCode, currentCode;
        currentCode = locationCodeTo;
        do {
            previousCode = cameFrom.get(currentCode);
            path.add(0, map.getRoad(previousCode, currentCode));
            currentCode = previousCode;
        } while (previousCode != locationCodeFrom);

        Path pathObject = new Path();
        pathObject.setRoads(new HashSet<>(path));
        return pathObject;
    }
}
