package taxiapp.core.models.logic.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taxiapp.core.models.entities.Path;
import taxiapp.core.models.entities.Road;
import taxiapp.core.models.entities.TaxiOrder;
import taxiapp.core.models.logic.interfaces.IOrdersProcessor;
import taxiapp.core.models.logic.interfaces.IPathFinder;

import java.util.Set;

/**
 * Created by urion on 27.10.15.
 */
@Component
public class OrdersProcessor implements IOrdersProcessor {

    IPathFinder pathFinder;

    @Autowired
    public OrdersProcessor(IPathFinder pathFinder) {
        this.pathFinder = pathFinder;
    }

    @Override
    public TaxiOrder addSystemInfoForOrder(TaxiOrder taxiOrderFromClient) {

        int from = taxiOrderFromClient.getLocationCodeFrom();
        int to = taxiOrderFromClient.getLocationCodeTo();
        Path path = pathFinder.findPath(from, to);

        taxiOrderFromClient.setPath(path);
        taxiOrderFromClient.setPrice(calculatePrice(path.getRoads()));
        return taxiOrderFromClient;
    }

    private double calculatePrice(Set<Road> roadList) {
        double price = 0.0;

        for (Road road : roadList)
            price += road.getLength();

        return price;
    }
}
