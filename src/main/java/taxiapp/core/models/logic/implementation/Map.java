package taxiapp.core.models.logic.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import taxiapp.core.DAL.implementation.LocationRepository;
import taxiapp.core.DAL.implementation.RoadRepository;
import taxiapp.core.models.entities.Road;
import taxiapp.core.models.logic.interfaces.IMap;

import java.util.List;
import java.util.Optional;

/**
 * Created by urion on 29.10.15.
 */
@Component
public class Map implements IMap {

    static final double inf = Double.MAX_VALUE / 2.0;
    private int n;
    private double[][] adjacencyMatrix;
    private RoadRepository roadRepository;
    private LocationRepository locationRepository;
    private boolean isInitialized = false;

    @Autowired
    public Map(RoadRepository roadRepository, LocationRepository locationRepository) {
        this.roadRepository = roadRepository;
        this.locationRepository = locationRepository;
    }

    private void initializeAdjacencyMatrix() {
        adjacencyMatrix = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                adjacencyMatrix[i][j] = inf;
            }
            adjacencyMatrix[i][i] = 0;
        }

        List<Road> roads = roadRepository.findAll();
        roads.forEach(road -> {
            int from = road.getStart().getCode();
            int to = road.getFinish().getCode();
            adjacencyMatrix[from][to] = road.getLength();
        });

    }

    private void initializeMap() {
        this.n = this.locationRepository.findAll().size();
        initializeAdjacencyMatrix();
        isInitialized = true;
    }

    @Override
    public double getLength(int from, int to) {
        if (!isInitialized) {
            initializeMap();
        }

        return adjacencyMatrix[from][to];
    }

    @Override
    public Road getRoad(int from, int to) {
        if (!isInitialized) {
            initializeMap();
        }

        Optional<Road> possibleRoad = roadRepository.findByPredicate(road -> road.getStart().getCode() == from &&
                road.getFinish().getCode() == to).stream().findFirst();
        if (possibleRoad.isPresent())
            return possibleRoad.get();
        else throw new NotImplementedException();
    }

    @Override
    public double INF() {
        return inf;
    }

    @Override
    public int N() {
        return n;
    }
}
