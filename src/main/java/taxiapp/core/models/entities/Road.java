package taxiapp.core.models.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by urion on 25.10.15.
 */
@Entity
public class Road {
    @Id @GeneratedValue
    private Long id;
    @ManyToOne
    private Location start;
    @ManyToOne
    private Location finish;

    private double length;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Road)) return false;

        Road road = (Road) o;

        if (Double.compare(road.length, length) != 0) return false;
        if (!start.equals(road.start)) return false;
        return finish.equals(road.finish);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = start.hashCode();
        result = 31 * result + finish.hashCode();
        temp = Double.doubleToLongBits(length);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Location getStart() {
        return start;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public Location getFinish() {
        return finish;
    }

    public void setFinish(Location finish) {
        this.finish = finish;
    }
}
