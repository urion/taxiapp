package taxiapp.core.models.entities;

import javax.persistence.*;

/**
 * Created by urion on 25.10.15.
 */
@Entity
public class TaxiOrder {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private Car car;
    @OneToOne
    private Path path;

    private String clientName;
    private int status;
    private int locationCodeFrom;
    private int locationCodeTo;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public int getLocationCodeFrom() {
        return locationCodeFrom;
    }

    public void setLocationCodeFrom(int locationCodeFrom) {
        this.locationCodeFrom = locationCodeFrom;
    }

    public int getLocationCodeTo() {
        return locationCodeTo;
    }

    public void setLocationCodeTo(int locationCodeTo) {
        this.locationCodeTo = locationCodeTo;
    }

    private double price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

//    @ManyToMany(
//            targetEntity=taxiapp.core.models.entities.Road.class,
//            cascade={CascadeType.PERSIST, CascadeType.MERGE}
//    )
//    @JoinTable(
//            name="ORDER_ROADS",
//            joinColumns=@JoinColumn(name="ID"),
//            inverseJoinColumns=@JoinColumn(name="ID")
//    )

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
