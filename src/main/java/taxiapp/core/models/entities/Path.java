package taxiapp.core.models.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

/**
 * Created by urion on 13.12.15.
 */
@Entity
public class Path {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToMany(targetEntity = Road.class)
    private Set roads;

    public Set getRoads() {
        return roads;
    }

    public void setRoads(Set roads) {
        this.roads = roads;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
