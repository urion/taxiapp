package taxiapp.core.models.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by urion on 25.10.15.
 */
@Entity
public class Location {
    @Id
    @GeneratedValue
    private Long id;
    private int code;

    @Override
    public String toString() {
        return "Location{" +
                "code=" + code +
                '}';
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
