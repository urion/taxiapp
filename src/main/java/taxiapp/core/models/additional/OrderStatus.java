package taxiapp.core.models.additional;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by urion on 25.10.15.
 */
public enum OrderStatus {
    InProcess, Completed, Rejected, Waiting;

    public static OrderStatus fromInt(int code) {
        switch (code) {
            case 0:
                return InProcess;
            case 1:
                return Completed;
            case 2:
                return Rejected;
            case 3:
                return Waiting;
        }
        throw new NotImplementedException();
        //return null;
    }

    public static int toInt(OrderStatus status) {
        switch (status) {
            case InProcess:
                return 0;
            case Completed:
                return 1;
            case Rejected:
                return 2;
            case Waiting:
                return 3;
        }
        return -1;
    }
}
