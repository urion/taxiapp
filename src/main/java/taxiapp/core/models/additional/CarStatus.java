package taxiapp.core.models.additional;

/**
 * Created by urion on 01.12.15.
 */
public enum CarStatus {
    Inactive, Available, Busy;

    public static CarStatus fromInt(int code) {
        switch (code) {
            case 0:
                return Inactive;
            case 1:
                return Available;
            case 2:
                return Busy;
        }
        return null;
    }

    public static int toInt(CarStatus status) {
        switch (status) {
            case Inactive:
                return 0;
            case Available:
                return 1;
            case Busy:
                return 2;
        }
        return -1;
    }
}
