# RESTful web app for java course #

## Functionality: ##
There are two roles in the system - customer and driver. Driver can register car and receive requests. Customer can create a request, get information about cost, car's positions etc. For managing traffic Dejkstra algorithm is used.

## Technologies: ##
* Spring
* Maven
* JPA
* Hibernate
* H2
* AngularJS
* Bower
* Grunt